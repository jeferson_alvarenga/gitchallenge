//
//  RepositoriesViewControllerTests.swift
//  GitTestTests
//
//  Created by Jeferson Alvarenga on 08/02/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import Foundation
import Quick
import Nimble
import KIF_Quick
import Nimble_Snapshots
@testable import GitTest

class RepositoriesViewControllerTests: KIFSpec {
    
    override func spec() {
        describe("RepositoriesViewController behavior") {
            var sut: RepositoriesViewController!
            var window: UIWindow!
            beforeEach {
                window = UIWindow(frame: CGRect(x:0, y: 20, width: 375, height: 792))
                window.makeKeyAndVisible()
            }
            afterEach {
                
            }
            context("on load .success") {
                beforeEach {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    sut = storyboard.instantiateViewController(withIdentifier: "RepositoriesViewController") as? RepositoriesViewController
                    window.rootViewController = sut
                }
                it("should load the git repositories") {
                    tester().waitForCell(at: IndexPath(item: 0, section: 0), in: sut.tableView)
                    //TODO apply this test on a static view
                    expect(sut.view) == recordSnapshot()
                    tester().tapRow(at: IndexPath(item: 0, section: 0), in: sut.tableView)
                }
            }
            context("on load .failed") {
                beforeEach {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    sut = storyboard.instantiateViewController(withIdentifier: "RepositoriesViewController") as? RepositoriesViewController
                    window.rootViewController = sut
                }
                //TODO do the error context failed
            }
        }
    }
}
