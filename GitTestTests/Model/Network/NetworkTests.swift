//
//  NetworkTests.swift
//  GitTestTests
//
//  Created by Jeferson Alvarenga on 11/02/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//
import XCTest
import ObjectMapper
import OHHTTPStubs
@testable import GitTest

class NetworkTests: GitTestTests {
    
    func testMappable() {
        let expectation = XCTestExpectation()
        stub(condition: isHost("api.github.com")) { _ in
            let stubPath = OHPathForFile("gitrequest.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
        }
        if let url = APIURL.repositories.composedURL(params: [1]) {
            Network.shared.mappable(url, entity: GitRequest.self, onSuccess: { (repositories) in
                expectation.fulfill()
                if let items = repositories.items, let repository = items.first {
                    XCTAssertNotNil(repository.name, "The repository name cannot be nil.")
                    XCTAssertNotNil(repository.description, "The repository description cannot be nil.")
                    XCTAssertNotNil(repository.login, "The author login cannot be nil.")
                    XCTAssertNotNil(repository.avatarURL, "The author avatar url cannot be nil.")
                } else {
                    XCTFail("The Repository model could not be created")
                }
            }, onFailure: { (error) in
                XCTFail("The Repository model could not be created")
            })
        }
        wait(for: [expectation], timeout: timeout)
        OHHTTPStubs.removeAllStubs()
    }
    
    func testMappables() {
        let expectation = XCTestExpectation()
        stub(condition: isHost("api.github.com")) { _ in
            let stubPath = OHPathForFile("pullrequests.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
        }
        if let url = APIURL.pullrequests.composedURL(params: ["AlamoFire", "AlamoFire"]) {
            Network.shared.mappables(url, entity: PullRequest.self, onSuccess: { (PRs) in
                expectation.fulfill()
                if let pullRequest = PRs.first {
                    XCTAssertNotNil(pullRequest.login, "The pull request login cannot be nil.")
                } else {
                    XCTFail("The Repository model could not be created")
                }
            }, onFailure: { (error) in
                XCTFail("The Repository model could not be created")
            })
        }
        wait(for: [expectation], timeout: timeout)
        OHHTTPStubs.removeAllStubs()
    }
}
