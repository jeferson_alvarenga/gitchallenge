//
//  PullRequestTests.swift
//  GitTestTests
//
//  Created by Jeferson Alvarenga on 11/02/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import Quick
import Nimble
import ObjectMapper

@testable import GitTest

class PullRequestTests: QuickSpec {
    
    override func spec() {
        var pullRequest: PullRequest!
        describe("The pull request") {
            context("Can create the PR model") {
                afterEach {
                    pullRequest = nil
                }
                beforeEach {
                    let json = "{ \"url\": \"https://api.github.com/repos/Alamofire/Alamofire/pulls/2715\", \"id\": 251836348, \"node_id\": \"MDExOlB1bGxSZXF1ZXN0MjUxODM2MzQ4\", \"html_url\": \"https://github.com/Alamofire/Alamofire/pull/2715\", \"diff_url\": \"https://github.com/Alamofire/Alamofire/pull/2715.diff\", \"patch_url\": \"https://github.com/Alamofire/Alamofire/pull/2715.patch\", \"issue_url\": \"https://api.github.com/repos/Alamofire/Alamofire/issues/2715\", \"number\": 2715, \"state\": \"open\", \"locked\": false, \"title\": \"Fix typo SessionManager.swift -> Session.swift\", \"user\": { \"login\": \"sunshinejr\", \"id\": 5232779, \"node_id\": \"MDQ6VXNlcjUyMzI3Nzk=\", \"avatar_url\": \"https://avatars3.githubusercontent.com/u/5232779?v=4\", \"gravatar_id\": \"\", \"url\": \"https://api.github.com/users/sunshinejr\", \"html_url\": \"https://github.com/sunshinejr\", \"followers_url\": \"https://api.github.com/users/sunshinejr/followers\", \"following_url\": \"https://api.github.com/users/sunshinejr/following{/other_user}\", \"gists_url\": \"https://api.github.com/users/sunshinejr/gists{/gist_id}\", \"starred_url\": \"https://api.github.com/users/sunshinejr/starred{/owner}{/repo}\", \"subscriptions_url\": \"https://api.github.com/users/sunshinejr/subscriptions\", \"organizations_url\": \"https://api.github.com/users/sunshinejr/orgs\", \"repos_url\": \"https://api.github.com/users/sunshinejr/repos\", \"events_url\": \"https://api.github.com/users/sunshinejr/events{/privacy}\", \"received_events_url\": \"https://api.github.com/users/sunshinejr/received_events\", \"type\": \"User\", \"site_admin\": false } }"
                    pullRequest = Mapper<PullRequest>().map(JSONString: json)
                }
                it("Can get the PR login.") {
                    expect(pullRequest?.login).to(equal("sunshinejr"))
                }
            }
        }
    }
}
