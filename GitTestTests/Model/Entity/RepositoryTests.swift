//
//  RepositoryTests.swift
//  GitTestTests
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import Quick
import Nimble
import ObjectMapper
@testable import GitTest

class RepositoryTests: QuickSpec {
    
    override func spec() {
        var repo: Repository!
        describe("The repository") {
            context("Can create the repository model") {
                afterEach {
                    repo = nil
                }
                beforeEach {
                    let json = "{ \"id\": 22458259, \"node_id\": \"MDEwOlJlcG9zaXRvcnkyMjQ1ODI1OQ==\", \"name\": \"Alamofire\", \"full_name\": \"Alamofire/Alamofire\", \"private\": false, \"owner\": { \"login\": \"Alamofire\", \"id\": 7774181, \"node_id\": \"MDEyOk9yZ2FuaXphdGlvbjc3NzQxODE=\", \"avatar_url\": \"https://avatars3.githubusercontent.com/u/7774181?v=4\", \"gravatar_id\": \"\", \"url\": \"https://api.github.com/users/Alamofire\", \"html_url\": \"https://github.com/Alamofire\", \"followers_url\": \"https://api.github.com/users/Alamofire/followers\", \"following_url\": \"https://api.github.com/users/Alamofire/following{/other_user}\", \"gists_url\": \"https://api.github.com/users/Alamofire/gists{/gist_id}\", \"starred_url\": \"https://api.github.com/users/Alamofire/starred{/owner}{/repo}\", \"subscriptions_url\": \"https://api.github.com/users/Alamofire/subscriptions\", \"organizations_url\": \"https://api.github.com/users/Alamofire/orgs\", \"repos_url\": \"https://api.github.com/users/Alamofire/repos\", \"events_url\": \"https://api.github.com/users/Alamofire/events{/privacy}\", \"received_events_url\": \"https://api.github.com/users/Alamofire/received_events\", \"type\": \"Organization\", \"site_admin\": false }, \"html_url\": \"https://github.com/Alamofire/Alamofire\", \"description\": \"Elegant HTTP Networking in Swift\", \"fork\": false, \"url\": \"https://api.github.com/repos/Alamofire/Alamofire\", \"forks_url\": \"https://api.github.com/repos/Alamofire/Alamofire/forks\", \"keys_url\": \"https://api.github.com/repos/Alamofire/Alamofire/keys{/key_id}\", \"collaborators_url\": \"https://api.github.com/repos/Alamofire/Alamofire/collaborators{/collaborator}\", \"teams_url\": \"https://api.github.com/repos/Alamofire/Alamofire/teams\", \"hooks_url\": \"https://api.github.com/repos/Alamofire/Alamofire/hooks\", \"issue_events_url\": \"https://api.github.com/repos/Alamofire/Alamofire/issues/events{/number}\", \"events_url\": \"https://api.github.com/repos/Alamofire/Alamofire/events\", \"assignees_url\": \"https://api.github.com/repos/Alamofire/Alamofire/assignees{/user}\", \"branches_url\": \"https://api.github.com/repos/Alamofire/Alamofire/branches{/branch}\", \"tags_url\": \"https://api.github.com/repos/Alamofire/Alamofire/tags\", \"blobs_url\": \"https://api.github.com/repos/Alamofire/Alamofire/git/blobs{/sha}\", \"git_tags_url\": \"https://api.github.com/repos/Alamofire/Alamofire/git/tags{/sha}\", \"git_refs_url\": \"https://api.github.com/repos/Alamofire/Alamofire/git/refs{/sha}\", \"trees_url\": \"https://api.github.com/repos/Alamofire/Alamofire/git/trees{/sha}\", \"statuses_url\": \"https://api.github.com/repos/Alamofire/Alamofire/statuses/{sha}\", \"languages_url\": \"https://api.github.com/repos/Alamofire/Alamofire/languages\", \"stargazers_url\": \"https://api.github.com/repos/Alamofire/Alamofire/stargazers\", \"contributors_url\": \"https://api.github.com/repos/Alamofire/Alamofire/contributors\", \"subscribers_url\": \"https://api.github.com/repos/Alamofire/Alamofire/subscribers\", \"subscription_url\": \"https://api.github.com/repos/Alamofire/Alamofire/subscription\", \"commits_url\": \"https://api.github.com/repos/Alamofire/Alamofire/commits{/sha}\", \"git_commits_url\": \"https://api.github.com/repos/Alamofire/Alamofire/git/commits{/sha}\", \"comments_url\": \"https://api.github.com/repos/Alamofire/Alamofire/comments{/number}\", \"issue_comment_url\": \"https://api.github.com/repos/Alamofire/Alamofire/issues/comments{/number}\", \"contents_url\": \"https://api.github.com/repos/Alamofire/Alamofire/contents/{+path}\", \"compare_url\": \"https://api.github.com/repos/Alamofire/Alamofire/compare/{base}...{head}\", \"merges_url\": \"https://api.github.com/repos/Alamofire/Alamofire/merges\", \"archive_url\": \"https://api.github.com/repos/Alamofire/Alamofire/{archive_format}{/ref}\", \"downloads_url\": \"https://api.github.com/repos/Alamofire/Alamofire/downloads\", \"issues_url\": \"https://api.github.com/repos/Alamofire/Alamofire/issues{/number}\", \"pulls_url\": \"https://api.github.com/repos/Alamofire/Alamofire/pulls{/number}\", \"milestones_url\": \"https://api.github.com/repos/Alamofire/Alamofire/milestones{/number}\", \"notifications_url\": \"https://api.github.com/repos/Alamofire/Alamofire/notifications{?since,all,participating}\", \"labels_url\": \"https://api.github.com/repos/Alamofire/Alamofire/labels{/name}\", \"releases_url\": \"https://api.github.com/repos/Alamofire/Alamofire/releases{/id}\", \"deployments_url\": \"https://api.github.com/repos/Alamofire/Alamofire/deployments\", \"created_at\": \"2014-07-31T05:56:19Z\", \"updated_at\": \"2019-01-23T15:41:13Z\", \"pushed_at\": \"2019-01-23T02:53:02Z\", \"git_url\": \"git://github.com/Alamofire/Alamofire.git\", \"ssh_url\": \"git@github.com:Alamofire/Alamofire.git\", \"clone_url\": \"https://github.com/Alamofire/Alamofire.git\", \"svn_url\": \"https://github.com/Alamofire/Alamofire\", \"homepage\": \"\", \"size\": 8234, \"stargazers_count\": 29992, \"watchers_count\": 29992, \"language\": \"Swift\", \"has_issues\": true, \"has_projects\": true, \"has_downloads\": true, \"has_wiki\": false, \"has_pages\": true, \"forks_count\": 5346, \"mirror_url\": null, \"archived\": false, \"open_issues_count\": 40, \"license\": { \"key\": \"mit\", \"name\": \"MIT License\", \"spdx_id\": \"MIT\", \"url\": \"https://api.github.com/licenses/mit\", \"node_id\": \"MDc6TGljZW5zZTEz\" }, \"forks\": 5346, \"open_issues\": 40, \"watchers\": 29992, \"default_branch\": \"master\", \"score\": 1.0 }"
                    repo = Mapper<Repository>().map(JSONString: json)
                }
                it("Can get the repsitory name.") {
                    expect(repo?.name).to(equal("Alamofire"))
                }
                it("Can get the repository description") {
                    expect(repo?.description).to(equal("Elegant HTTP Networking in Swift"))
                }
                it("Can get the user login"){
                    expect(repo?.login).to(equal("Alamofire"))
                }
                it("Can get the avatar URL") {
                    expect(repo?.avatarURL).to(equal("https://avatars3.githubusercontent.com/u/7774181?v=4"))
                }
            }
        }
    }
}
