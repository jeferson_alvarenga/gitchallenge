//
//  GitErrorTests.swift
//  GitTestTests
//
//  Created by Jeferson Alvarenga on 11/02/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import Quick
import Nimble
import ObjectMapper
@testable import GitTest

class GitErrorTests: QuickSpec {

    override func spec() {
        var error: GitError!
        describe("The error class") {
            context("Can create the error model") {
                afterEach {
                    error = nil
                }
                beforeEach {
                    error = GitError(type: .database, title: "Error title test")
                }
                it("Can get the error title.") {
                    expect(error?.title).to(equal("Error title test"))
                }
            }
        }
    }
}
