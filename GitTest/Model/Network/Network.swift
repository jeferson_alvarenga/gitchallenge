//
//  Network.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum APIURL {
    case repositories
    case pullrequests
    
    func composedURL(params: [Any]) -> String? {
        switch self {
        case .repositories:
            if let page = params[0] as? Int {
                return "https://api.github.com/search/repositories?q=language:Swift&sort=stars&page=\(page)"
            } else {
                return nil
            }
        case .pullrequests:
            if let login = params[0] as? String,
                let repository = params[1] as? String {
                return "https://api.github.com/repos/\(login)/\(repository)/pulls"
            } else {
                return nil
            }
        }
    }
}

class Network {
    static let shared = Network()
    typealias onCompletion = (Any) -> Void
    typealias onCompletionFailure = (GitError) -> Void
    
    func mappable<T: Mappable>(_ url: String,
                               entity: T.Type,
                               parameters: Any? = nil,
                               method: String? = "GET",
                               onSuccess: @escaping (T) -> Void,
                               onFailure: @escaping onCompletionFailure) {
        
        if !checkInternetConnection() {
            let error = GitError(
                type: .noInternet,
                title: "Falha ao executar requisição. Sem conexão com a internet",
                detail: url)
            onFailure(error)
        } else {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            do {
                var request = URLRequest(url: try url.asURL())
                
                //Parameters
                if let params = parameters {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params)
                }
                
                //Method
                var httpMethod = HTTPMethod.get
                if let parameterMethod = method, let vMethod = HTTPMethod(rawValue: parameterMethod) {
                    httpMethod = vMethod
                }
                request.httpMethod = httpMethod.rawValue
                
                Alamofire.request(request).validate().responseObject { (response: DataResponse<T>) in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    if response.result.isSuccess {
                        if let value = response.result.value {
                            onSuccess(value as T)
                        } else {
                            let error = GitError(
                                type: .serialization,
                                title: "Falha ao executar requisição (Response.result.value is null).",
                                detail: url)
                            onFailure(error)
                        }
                    } else {
                        let error = GitError(
                            type: .serialization,
                            title: "Falha ao executar requisição HTTP",
                            detail: "\(response.result.error?.localizedDescription ?? "") URL: \(url)",
                            error: response.result.error)
                        onFailure(error)
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                let error = GitError(
                    type: .serialization,
                    title: "Falha ao parametrizar requisição",
                    detail: url)
                onFailure(error)
            }
        }
    }
    
    func mappables<T: Mappable>(_ url: String,
                                entity: T.Type,
                                parameters: Any? = nil,
                                method: String? = "GET",
                                onSuccess: @escaping ([T]) -> Void,
                                onFailure: @escaping onCompletionFailure) {
        
        if !checkInternetConnection() {
            let error = GitError(
                type: .noInternet,
                title: "Falha ao executar requisição. Sem conexão com a internet",
                detail: url)
            onFailure(error)
        } else {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            do {
                var request = URLRequest(url: try url.asURL())
                
                //Parameters
                if let params = parameters {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params)
                }
                
                //Method
                var httpMethod = HTTPMethod.get
                if let parameterMethod = method, let vMethod = HTTPMethod(rawValue: parameterMethod) {
                    httpMethod = vMethod
                }
                request.httpMethod = httpMethod.rawValue
                
                Alamofire.request(request).validate().responseArray { (response: DataResponse<[T]>) in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    if response.result.isSuccess {
                        if let value = response.result.value {
                            onSuccess(value as [T])
                        } else {
                            let error = GitError(
                                type: .serialization,
                                title: "Falha ao executar requisição (Response.result.value is null).",
                                detail: url)
                            onFailure(error)
                        }
                    } else {
                        let error = GitError(
                            type: .serialization,
                            title: "Falha ao executar requisição. (Response.result.isFailure).",
                            detail: url,
                            error: response.result.error)
                        onFailure(error)
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                let error = GitError(
                    type: .serialization,
                    title: "Falha ao parametrizar requisição",
                    detail: url)
                onFailure(error)
            }
        }
    }
    
    func checkInternetConnection() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
