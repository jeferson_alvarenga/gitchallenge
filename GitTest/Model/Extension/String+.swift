//
//  String+.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 24/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import Foundation

extension String {
    var localizedString: String {
        return NSLocalizedString(self, comment: "")
    }
    
    var dateFormat: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.locale = Locale.current
        if let date = formatter.date(from: self) {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            return formatter.string(from: date)
        }
        return self
    }
}
