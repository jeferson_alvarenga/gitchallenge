//
//  Repository.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import ObjectMapper

class Repository: Mappable {
    var name: String?
    var description: String?
    var login: String?
    var avatarURL: String?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        login <- map["owner.login"]
        avatarURL <- map["owner.avatar_url"]
    }
}
