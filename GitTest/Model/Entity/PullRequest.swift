//
//  PullRequest.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import ObjectMapper

class PullRequest: Mappable {
    var login: String?
    var title: String?
    var date: String?
    var avatarURL: String?
    var url: String?
    
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        login <- map["user.login"]
        avatarURL <- map["user.avatar_url"]
        title <- map["title"]
        date <- map["created_at"]
        url <- map["html_url"]
    }
}

