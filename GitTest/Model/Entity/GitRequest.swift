//
//  GitRequest.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import ObjectMapper

class GitRequest: Mappable {
    var items: [Repository]?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        items <- map["items"]
    }
}
