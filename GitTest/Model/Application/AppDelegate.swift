//
//  AppDelegate.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

