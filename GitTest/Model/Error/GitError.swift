//
//  GitError.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit

enum GitErrorType {
    case database
    case noResult
    case noInternet
    case serialization
}

class GitError: Error {
    let error: Error?
    let type: GitErrorType
    let source: String
    let title: String
    let detail: String?
    
    init(type: GitErrorType, file: StaticString = #file, function: StaticString = #function, line: UInt = #line, title: String, detail: String? = nil, error: Error? =  nil) {
        self.error = error
        self.type = type
        self.source = URL(fileURLWithPath: file.description).lastPathComponent + ":" + function.description + ", line \(line)"
        self.title = title
        self.detail = detail
    }
}
