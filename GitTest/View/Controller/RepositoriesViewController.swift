//
//  RepositoriesViewController.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RepositoriesViewController: BaseViewController, RepositoriesViewModel, RepositoriesProtocol {
    
    var delegate: RepositoriesProtocol!
    var repositories: Variable<[Repository]> = Variable([])
    var currentPage: Int = 0
    var isFetchInProgress: Bool = false
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(handleRefresh(_:)),for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Repositories".localizedString
        tableView.accessibilityIdentifier = "tableView"
        tableView.addSubview(refreshControl)
        delegate = self
        setupCellConfiguration()
        setupCellTapHandling()
        loadItems()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadItems()
    }
    
    func setupCellConfiguration() {
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        tableView.rx.setPrefetchDataSource(self).disposed(by: disposeBag)
        repositories.asObservable()
            .bind(to: tableView
                .rx
                .items(cellIdentifier: RepositoryCellTableViewCell.Identifier, cellType: RepositoryCellTableViewCell.self)) {
                    row, repository, cell in
                    cell.configure(repository: repository)
            }
            .disposed(by: disposeBag)
    }
    
    func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(Repository.self)
            .subscribe(onNext: {
                repository in
                if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
                self.performSegue(withIdentifier: "segueShowPullRequests", sender: repository)
            })
            .disposed(by: disposeBag)
    }
    
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]) {
        refreshControl.endRefreshing()
        let visibleToReload = visibleIndexPathsToReload(intersecting: newIndexPathsToReload)
        tableView.reloadRows(at: visibleToReload, with: .automatic)
        tableView.tableFooterView = nil
    }
    
    func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
        let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows ?? []
        let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
        return Array(indexPathsIntersection)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueShowPullRequests" {
            if let viewController = segue.destination as? PullRequestViewController,
                let repository = sender as? Repository {
               viewController.repository = repository
            }
        }
    }
}

extension RepositoriesViewController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            loadItems()
        }
    }
}

extension RepositoriesViewController {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offset: CGPoint = scrollView.contentOffset
        let bounds: CGRect = scrollView.bounds
        let size: CGSize = scrollView.contentSize
        let inset: UIEdgeInsets = scrollView.contentInset
        let y: CGFloat = offset.y + bounds.size.height - inset.bottom
        let h: CGFloat = size.height
        let reloadDistance: CGFloat = 10
        if y > h + reloadDistance {
            loadItems()
        }
    }
}
