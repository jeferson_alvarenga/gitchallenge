//
//  PullRequestViewController.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 24/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PullRequestViewController: BaseViewController, PullRequestViewModel, PullRequestProtocol {
    
    var delegate: PullRequestProtocol!
    var pullRequests: Variable<[PullRequest]> = Variable([])
    var repository: Repository!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = repository.name?.appending(" Pull requests")
        delegate = self
        setupCellConfiguration()
        setupCellTapHandling()
        loadItems()
    }
    
    func setupCellConfiguration() {
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        pullRequests.asObservable()
            .bind(to: tableView
                .rx
                .items(cellIdentifier: PullRequestTableViewCell.Identifier, cellType: PullRequestTableViewCell.self)) {
                    row, pullRequest, cell in
                    cell.configure(pullRequest: pullRequest)
            }
            .disposed(by: disposeBag)
    }
    
    func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(PullRequest.self)
            .subscribe(onNext: {
                pullRequest in
                if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
                if let pullURL = pullRequest.url, let url = URL(string: pullURL), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            })
            .disposed(by: disposeBag)
    }
    
    func onFetchCompleted() {
        tableView.tableFooterView = nil
        tableView.reloadData()
    }
}
