//
//  BaseViewController.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 24/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class BaseViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let disposeBag = DisposeBag()
    
    func onFetchError(error: GitError, iconImageName: String? = "Arrow") {
        let viewError = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 130))
        let lblError = UILabel(frame: viewError.frame)
        lblError.font = UIFont.systemFont(ofSize: 14.0)
        lblError.textAlignment = .center
        let errorText = NSMutableAttributedString(string: error.title)
        if let icon = iconImageName {
            let imageAttachment =  NSTextAttachment()
            imageAttachment.image = UIImage(named: icon)
            imageAttachment.bounds = CGRect(x: 10, y: -5.0, width: 20, height: 20)
            let attachmentString = NSAttributedString(attachment: imageAttachment)
            errorText.append(attachmentString)
        }
        lblError.attributedText = errorText
        viewError.addSubview(lblError)
        tableView.tableFooterView = viewError
    }
    
    func addLoadingFooter() {
        tableView.tableFooterView = nil
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(130))
        tableView.tableFooterView = spinner
        tableView.tableFooterView?.isHidden = false
    }
}

extension BaseViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
}
