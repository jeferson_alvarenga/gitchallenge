//
//  RepositoryCellTableViewCell.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit
import AlamofireImage

class RepositoryCellTableViewCell: UITableViewCell {

    static let Identifier = "RepositoryTableViewCell"
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var txtDescription: UITextView!
    
    func configure(repository: Repository) {
        lblName.text = repository.name
        lblLogin.text = repository.login
        txtDescription.text = repository.description
        txtDescription.isUserInteractionEnabled = false
        if let avatarURL = repository.avatarURL,
            let url = URL(string: avatarURL) {
            imgAvatar.af_setImage(withURL: url, placeholderImage: UIImage(named: "Avatar"))
        }
    }
}
