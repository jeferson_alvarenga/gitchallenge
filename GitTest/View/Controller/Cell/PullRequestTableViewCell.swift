//
//  PullRequestTableViewCell.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 24/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import UIKit
import AlamofireImage

class PullRequestTableViewCell: UITableViewCell {
    
    static let Identifier = "PullRequestTableViewCell"
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    
    func configure(pullRequest: PullRequest) {
        lblTitle.text = pullRequest.title
        lblLogin.text = pullRequest.login
        lblDate.text = pullRequest.date?.dateFormat
        if let avatarURL = pullRequest.avatarURL,
            let url = URL(string: avatarURL) {
            imgAvatar.af_setImage(withURL: url)
        }
    }
}

