//
//  PullRequestViewModel.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 24/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import RxSwift

protocol PullRequestProtocol: BaseProtocol {
    func onFetchCompleted()
}

protocol PullRequestViewModel: class {
    var pullRequests: Variable<[PullRequest]> {get set}
    var delegate: PullRequestProtocol! {get set}
    var repository: Repository! {get set}
}

extension PullRequestViewModel {
    
    func loadItems() {
        if let login = self.repository.login,
            let name = repository.name,
            let url = APIURL.pullrequests.composedURL(params: [login, name]) {
            delegate.addLoadingFooter()
            Network.shared.mappables(url, entity: PullRequest.self, onSuccess: { (pullRequests) in
                if pullRequests.count > 0 {
                    self.pullRequests.value.append(contentsOf: pullRequests)
                    self.delegate.onFetchCompleted()
                } else {
                    self.delegate.onFetchError(error: GitError(type: .noResult, title: "No results found".localizedString), iconImageName: nil)
                }
            }, onFailure: { (gitError) in
                self.delegate.onFetchError(error: gitError, iconImageName: nil)
            })
        } else {
            self.delegate.onFetchError(error: GitError(type: .serialization, title: "Error trying to request data".localizedString), iconImageName: nil)
        }
    }
}
