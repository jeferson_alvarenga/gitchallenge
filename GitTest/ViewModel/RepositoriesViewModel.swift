//
//  RepositoriesViewModel.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 23/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//

import RxSwift

protocol RepositoriesProtocol: BaseProtocol {
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath])
}

protocol RepositoriesViewModel: class {
    var repositories: Variable<[Repository]> {get set}
    var currentPage: Int {get set}
    var isFetchInProgress: Bool {get set}
    var delegate: RepositoriesProtocol! {get set}
}

extension RepositoriesViewModel {
    
    func loadItems() {
        if !isFetchInProgress {
            isFetchInProgress = true
            delegate.addLoadingFooter()
            if let url = APIURL.repositories.composedURL(params: [currentPage + 1]) {
                Network.shared.mappable(url, entity: GitRequest.self, onSuccess: { (gitRequest) in
                    self.isFetchInProgress = false
                    if let items = gitRequest.items, items.count > 0 {
                        self.repositories.value.append(contentsOf: items)
                        self.currentPage += 1
                        let indexToLoad = self.calculateIndexPathsToReload(from: items)
                        self.delegate.onFetchCompleted(with: indexToLoad)
                    }
                }, onFailure: { (gitError) in
                    self.isFetchInProgress = false
                    self.delegate.onFetchError(error: gitError, iconImageName: "Arrow")
                })
            } else {
                self.delegate.onFetchError(error: GitError(type: .serialization, title: "Error trying to request data".localizedString), iconImageName: "Arrow")
            }
        }
    }
    
    func calculateIndexPathsToReload(from newModerators: [Repository]) -> [IndexPath] {
        let startIndex = repositories.value.count - newModerators.count
        let endIndex = startIndex + newModerators.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
    
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        return indexPath.row >= (repositories.value.count-1)
    }
}
