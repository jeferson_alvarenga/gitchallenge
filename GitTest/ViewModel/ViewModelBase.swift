//
//  ViewModelBase.swift
//  GitTest
//
//  Created by Jeferson Alvarenga on 24/01/19.
//  Copyright © 2019 DeepLogical. All rights reserved.
//
import Foundation

protocol BaseProtocol {
    func onFetchError(error: GitError, iconImageName: String?)
    func setupCellTapHandling()
    func setupCellConfiguration()
    func addLoadingFooter()
}
